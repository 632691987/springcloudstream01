package com.example.loggingconsumer.config;

import com.example.loggingconsumer.dto.Person;
import java.util.function.Function;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {

//  @Bean
//  public Function<String, String> uppercase() {
//    return String::toUpperCase;
//  }
  // 只能有一个这样的 function, 有上面的就没有下面的
//  @Bean
//  public Function<Person, String> getPersonName() {
//    return Person::getName;
//  }
}
