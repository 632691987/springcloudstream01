package com.example.loggingconsumer;

import static org.assertj.core.api.Assertions.assertThat;

import com.example.loggingconsumer.dto.Person;
import javax.annotation.Resource;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.stream.binder.test.InputDestination;
import org.springframework.cloud.stream.binder.test.OutputDestination;
import org.springframework.cloud.stream.binder.test.TestChannelBinderConfiguration;
import org.springframework.context.annotation.Import;
import org.springframework.messaging.support.GenericMessage;

@SpringBootTest(classes = LoggingConsumerApplication.class)
@Import({TestChannelBinderConfiguration.class})
class LoggingConsumerApplicationTest {

  @Resource
  private InputDestination input;

  @Resource
  private OutputDestination output;

//  @Test
//  void testSendString() {
//    input.send(new GenericMessage<>("hello".getBytes()));
//    assertThat(output.receive().getPayload()).isEqualTo("HELLO".getBytes());
//  }

  @Test
  void testSendPerson() {
    String name = "vincent";
    Person p = new Person();
    p.setName(name);

    input.send(new GenericMessage<>(p));
    assertThat(output.receive().getPayload()).isEqualTo(name.getBytes());
  }
}
